import { ADD_BASKET } from "actions/basket-actions"

/**
 * 
 * redux ile sepete ekleme - çıkarma işlemleri burada yapılmaktadır.
 */

export default function updateBasket(state = {}, {type, payload}) {
    switch (type) {
      case ADD_BASKET:
        if (state.basket && state.basket.length > 0) {
          state.basket.push(payload.basket)
        } else {
          state.basket = [payload.basket]
        }
        return state
      case "clear":
        state.basket = []
        return state
      default:
        break
    }
    return state
  }