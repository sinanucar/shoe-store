import Card from "../Card/Card"
import React from "react"
import {
  ButtonBack,
  ButtonNext,
  CarouselProvider,
  DotGroup,
  Slide,
  Slider,
} from "pure-react-carousel"
import "pure-react-carousel/dist/react-carousel.es.css"

/**
 * Ürünün fotoğrafları kullanıcıya bu alanda gösterilmektedir.
 * Carousel için "pure-react-carousel" modulü kullanılmıştır.
 * Gösterilecek görseller "images" propu ile aktarılmıştır.
 */
class Showroom extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentSlideIndex: 0,
      width: 400,
      height: 400,
    }
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
  }

  componentDidMount() {
    this.updateWindowDimensions()
    window.addEventListener("resize", this.updateWindowDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions)
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }

  UpdateCurrentSlideIndex = (item) => {
    this.setState({
      currentSlideIndex: item,
    })
  }
  carouselOnChange = (event) => {
    console.log(event)
  }
  render() {
    return (
      <Card className={this.props.className}>
        <CarouselProvider
          visibleSlides={1}
          totalSlides={this.props.images.length}
          step={1}
          dragStep={1}
          currentSlide={this.state.currentSlideIndex}
          naturalSlideWidth={400}
          naturalSlideHeight={
            this.state.width > 640
              ? this.state.height * 0.3
              : this.state.height * 0.8
          }
          infinite={true}
        >
          <Slider>
            {this.props.images.map((t, index) => (
              <Slide tag="a" index={index} key={"slide_" + index}>
                <img
                  className="object-contain w-full h-full"
                  src={t}
                  alt=""
                ></img>
              </Slide>
            ))}
          </Slider>
          <div className="flex">
            <ButtonBack>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="black"
                width="18px"
                height="18px"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M11.67 3.87L9.9 2.1 0 12l9.9 9.9 1.77-1.77L3.54 12z" />
              </svg>
            </ButtonBack>
            {this.props.images.map((t, index) => (
              <Slide tag="a" index={index} key={"thumbnail_" + index}>
                <img
                  onClick={() => this.UpdateCurrentSlideIndex(index)}
                  className="object-contain w-full h-full"
                  src={t}
                  alt=""
                ></img>
              </Slide>
            ))}
            <ButtonNext>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="black"
                width="18px"
                height="18px"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z" />
              </svg>
            </ButtonNext>
          </div>
          <DotGroup />
        </CarouselProvider>
      </Card>
    )
  }
}

export default Showroom
