import React from "react"
import SearchBar from "./NavbarElements/SearchBar"
import Basket from "./NavbarElements/Basket"
import Images from "../../Statics/Images"

class Navbar extends React.Component {
  render() {
    return (
      <header className="flex items-center justify-between px-4 py-3 mb-3 border-b border-gray-300 shadow">
        <div className="">
          <img className="h-10" src={Images.logo} alt="Workcation" />
        </div>
        <div className="md:w-1/2">
        <SearchBar></SearchBar>
        </div>
        <div className="">
            <Basket></Basket>
        </div>
      </header>
    )
  }
}

export default Navbar
