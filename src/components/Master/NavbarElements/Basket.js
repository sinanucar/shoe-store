import React from "react"
import store from "../../../store"
import { connect } from "react-redux"
import Modal from "react-modal"

const _dimensions = { width: window.innerWidth, height: window.innerHeight }

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    borderRadius: 10,
    minWidth: _dimensions.width / 2,
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
}
class Basket extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      productCount: 0,
      products: [],
      show: false,
    }
  }
  handleClose = () => {
    this.setState({
      show: false,
    })
  }

  openBasket = () => {
    this.setState({
      show: true,
    })
  }

  render() {
    store.subscribe((result) => {
      //Storedaki değişikler takip edilip ona göre değişmesi gereken datalar değişiyor.
      //sepet içerisindeki ürünler kullanıcıya bu adımda tekrar gösteriliyor
      if (this.props.basket.basket)
        this.setState({
          productCount: this.props.basket.basket.length,
          products: this.props.basket.basket,
        })
    })
    return (
      <div>
        <button
          onClick={() => {
            this.openBasket()
          }}
          className="bg-gray-900 rounded-lg leading-none text-white p-3 px-4 focus:outline-none focus:shadow-outline"
        >
          <svg
            className="fill-current md:mr-3 text-white inline-block h-4 w-4"
            xmlns="http://www.w3.org/2000/svg"
            enableBackground="new 0 0 24 24"
            viewBox="0 0 24 24"
            fill="black"
            width="18px"
            height="18px"
          >
            <g>
              <rect fill="none" height="24" width="24" />
              <path d="M18,6h-2c0-2.21-1.79-4-4-4S8,3.79,8,6H6C4.9,6,4,6.9,4,8v12c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V8C20,6.9,19.1,6,18,6z M10,10c0,0.55-0.45,1-1,1s-1-0.45-1-1V8h2V10z M12,4c1.1,0,2,0.9,2,2h-4C10,4.9,10.9,4,12,4z M16,10c0,0.55-0.45,1-1,1 s-1-0.45-1-1V8h2V10z" />
            </g>
          </svg>
          <span className="hidden md:inline">Sepet </span>
          {this.state.productCount}
        </button>

        <Modal
          isOpen={this.state.show}
          onRequestClose={this.handleClose}
          style={customStyles}
          contentLabel="Sepetteki Ürünler"
        >
          <div className="flex flex-col">
            {this.state.productCount > 0 ? (
              <div className="text-gray-900 text-left px-4 py-2 m-2">
                <ul className="list-disc">
                  {this.state.products.map((t, index) => (
                    <li key={"product_" + index}>{t.name}</li>
                  ))}
                </ul>
              </div>
            ) : (
              <div className="text-gray-900 text-left px-4 py-2 m-2">
                Sepetinizde hiç ürün bulunmamaktadır!
              </div>
            )}

            <div className="px-2 py-1 mx-1">
              <button
                className="w-full px-2 py-1 border-red-700 border-2 rounded-lg text-red font-semibold"
                onClick={() => this.handleClose()}
              >
                Kapat
              </button>
            </div>
            {this.state.productCount > 0 ? (
              <div className="px-2 py-1 mx-1">
                <button className="w-full px-2 py-1 text-white text-lg rounded-lg bg-green-600 font-semibold">
                  {this.state.productCount} Ürün için ödeme yap
                </button>
              </div>
            ) : (
              <></>
            )}
          </div>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps)(Basket)
