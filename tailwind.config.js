module.exports = {
  theme: {
    extend: {
      textColor: {
        red: "#e0051f",
      },
      backgroundColor: {
        red: "#e0051f",
      },
      borderColor: {
        red: "#e0051f",
      },
    },
  },
  variants: {},
  plugins: [],
}
