import React from "react"
import Card from "components/Card/Card"

//Ürünün ayrıntılı bilgisi burada veriliyor.
//Ürünle ilgili değerlendirmeler ve iade ile ilgili alanlar şu an için işlevsiz.

class Details extends React.Component {

  constructor(props) {
    super(props)
    this.state = { selectedTab: "info" }
  }

  render() {
    return (
      <Card>
          <ul className="flex border-b mx-2">
            <li className="mr-1" >
              <p
                className="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-black font-semibold"
              >
                Ürün Detayları
              </p>
            </li>
            <li className="mr-1" >
              <p
                className="bg-white inline-block py-2 px-4 text-gray-500 hover:text-gray-800 font-semibold"
              >
                Değerlendirmeler
              </p>
            </li>
            <li className="mr-1" >
              <p
                className="bg-white inline-block py-2 px-4 text-gray-500 hover:text-gray-800 font-semibold"
              >
                İade Politikası
              </p>
            </li>
          </ul>
          <Card>
            <p className="p-2">{this.props.mainInfo}</p>
            <ul className="list-disc list-inside">
              {
                this.props.moreInfo.map((t,index) => (<li key={"info_"+index} className="m-2">{t}</li>))
              }
            </ul>
          </Card>
      </Card>
    )
  }
}

export default Details
