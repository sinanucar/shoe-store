import React from "react"
import Navbar from "./Navbar"
import Footer from "./Footer"
//sitede kullanılacak layout burada tanımlandı.
class Main extends React.Component {
  render() {
    return (
      <div className="flex flex-col h-screen justify-between">
        <Navbar className="h-10" />
        <main className="mx-auto container h-full">
          <div className={this.props.className}>{this.props.children}</div>
        </main>
        <Footer />
      </div>
    )
  }
}

export default Main
