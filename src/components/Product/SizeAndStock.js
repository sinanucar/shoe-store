import React from "react"

/**
 * Beden ve stok gösterim alanı.
 * 
 */
class SizeAndStock extends React.Component {
  constructor(props) {
    super(props)
    this.state = { selectedSize: null }
    this.ChooseSize = this.ChooseSize.bind(this)
  }
  ChooseSize(item) {
    this.setState({
      selectedSize: item,
    })
    this.props.onChange(item)
  }
  /**
   * props ile aktarılan "stokInfo" {id, stock} objesini içeren bir liste halinde aktarılmaktadır.
   *  "id" bilgisi ayakkabının numarasını belirtmektedir.
   * "stock" bilgisi 0 olan numaraların butonları disable edilmiştir.
   * Kullanıcının bu numaralar ile etkileşime girmesi engellenmiştir.
   * 
   */
  render() {
    let self = this
    if (this.props.stokInfo && this.props.stokInfo.length > 0) {
      return (
        <div>
          <p className="m-2">Ayakkabı Numarası</p>
          <div className="flex flex-wrap">
            {this.props.stokInfo.map((t,index) => {
              let className =
                "w-full font-bold border-2 py-2 px-4 rounded"
              if (this.state.selectedSize && t.id === this.state.selectedSize.id)
                className += " bg-gray-300 border-black"
                else
                className += " border-gray-500"
              return (
                <div key={"size_"+index}className="w-1/4 p-2">
                  {
                  t.stock > 0 ? (
                    <button onClick={() => {self.ChooseSize(t)}} className={className}>
                      {t.id}
                    </button>
                  ) : (
                    <button
                      onClick={() => {
                        self.ChooseSize(t)
                      }}
                      disabled
                      className="w-full border line-through cursor-not-allowed border-gray-500 text-gray-500 rounded py-2 px-4"
                    >
                      {t.id}
                    </button>
                  )}
                </div>
              )
            })}
          </div>
        </div>
      )
    } else {
      return (
        <div
          className="border border-red-500 text-red-700 px-4 py-3 rounded relative"
          role="alert"
        >
          <span className="block sm:inline">
            Ürün şu an için tedarik edilemiyor.
          </span>
        </div>
      )
    }
  }
}

export default SizeAndStock
