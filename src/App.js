import React from "react"
import Main from "./components/Master/Main"
import Card from "./components/Card/Card"
import Showroom from "components/Showroom/Showroom"
import Api from "api"
import Details from "components/Product/Details"
import Info from "components/Product/Info"
import Alert from "components/Alert/Alert"

class App extends React.Component {
  state = {
    product: null,
    productInfo: {},
  }

  componentDidMount() {
    //sayfa yüklendiğinde ilgili ürünün bilgileri çekilir.
    Api.getProduct()
      .then((result) => {
        this.setState({
          product: result,
        })
      })
      .catch((error) => {})
  }

  render() {
    return (
      <div>
        {this.state.product ? (
          <Main className="">
            <div className="grid md:grid-cols-0 lg:grid-cols-3 gap-1">
              <div className="col-span-2">
                <Showroom images={this.state.product.images}></Showroom>
              </div>
              <Info info={this.state.product}></Info>
            </div>
            <Details
              mainInfo={this.state.product.mainInfo}
              moreInfo={this.state.product.moreInfo}
            ></Details>
          </Main>
        ) : (
          <Main className="p-2">
            <Card>Sayfa yüklenemedi.</Card>
          </Main>
        )}
        <Alert />
      </div>
    )
  }
}
export default App
