/**
 * Site içerisinde kullanılan sabit görseller
 */

const Images = {
    logo: require("../images/show_store_icon.png"),
    logo_bg: require("../images/shoe_store_icon_v2.png"),
    logo_text: require("../images/logo_with_text.png")
}

export default Images