export const ADD_BASKET = "ADD_BASKET";

export function updateBasket(basketData){
    return {
        type: ADD_BASKET,
        payload: {
            basket : basketData
        }
    }
}