export function initAlert(alertData){
    return {
        type: "INIT_ALERT",
        payload: {
            alert : alertData
        }
    }
}
