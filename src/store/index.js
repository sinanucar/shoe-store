import { combineReducers, createStore } from "redux"
import updateBasket from "reducer/basket-reducer"
import initAlert from "reducer/alert-reducer"

// gerekli tüm reducer metodları burada tanımlandı
const rootReducer = combineReducers({
  basket: updateBasket,
  alert: initAlert
})

var defaultStore = {
  basket: [],
  alert: {}
}

/**
 * tanımlanan verilerin tutulacağı store alanı tanımlandı.
 * 
 */
const store = createStore(rootReducer, defaultStore)
store.subscribe( (state) => {
    Promise.resolve(state)
})

export default store


