/**
 * redux alert için tanımlama yapılmaktadır.
 */
export default function updateAlert(state = {}, { type, payload }) {
  if (payload) state.alert = payload.alert
  return state
}
