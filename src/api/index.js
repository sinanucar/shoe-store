//Http requestleri yapmak için hazırlandı.
import axios from "axios"

export default class Api {
  static async getProduct() {
    return axios
      .get("data.json").then((result) => {
        // console.log("ürün bilgileri => ", result.data)
        if (result && result.status === 200)
            return result.data
        else
            return Promise.reject("Beklenmeyen bir durum ile karşılaşıldı.")
    }).catch(err => {
        return Promise.reject("Beklenmeyen bir durum ile karşılaşıldı.")
    })
  }
}
