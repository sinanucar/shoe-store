import React from "react"

class SearchBar extends React.Component {
  render() {
    return (
      <div className="w-full flex">
        <input className="p-2 flex-grow border-2 border-gray-900 rounded-lg rounded-r-none focus:outline-none focus:shadow-outline" placeholder="Ürün ara..."></input>
        <button className="flex-none border-2 border-gray-900 bg-gray-900 rounded-lg rounded-l-none hover:rounded-lg hover:rounded-l-none text-white p-2 focus:outline-none focus:shadow-outline">Ara</button>
      </div>
    )
  }
}

export default SearchBar
