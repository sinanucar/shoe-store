import Card from "components/Card/Card"
import React from "react"
import SizeAndStock from "./SizeAndStock"

import { connect } from "react-redux"
import { updateBasket } from "actions/basket-actions"
import { initAlert } from "actions/alert-actions"

import ConfirmModal from "./ComfirmModal"

/*
 * Ürünün satın alım aşaması ile ilgili bilgiler burada verildi.
 * Satın alım aşaması da burada tanımlandı.
 */
class Info extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      price: 0,
      selectedSize: null,
    }
    this.addToBasket = this.addToBasket.bind(this)
  }

  /**
   * info ile ürünün tüm bilgileri aktarılmıştır.
   * state içerisindeki "price" bilgis ile kullanıcının bu ürün ödeyeceği toplam tutar tutuluyor.
   */
  componentDidMount() {
    let price = this.props.info.price
    let discount = this.props.info.discount
    if (discount && discount > 0) {
      let newPrice = (price * (100 - discount)) / 100
      this.setState({
        price: newPrice,
      })
    } else {
      this.setState({
        price: price,
      })
    }
  }

  /*
  * Ürünün sepete eklenmesi burada yapılıyor.
  * Ürün sepete eklenmeden önce, seçili beden bilgisi de ürüne ekleniyor.
  * Ayrıca eklendi uyarısı da burada tetikleniyor. 
  */
  addToBasket = () => {
    this.props.dispatch(
      updateBasket({
        ...this.props.info,
        ...{ selectedSize: this.state.selectedSize },
      })
    )
    this.props.dispatch(
      initAlert({ message: this.props.info.name, show: true })
    )
  }

  //beden bilgisinde değişiklikler burada izleniyor.
  sizeChanged = (item) => {
    this.setState({
      selectedSize: item,
    })
  }

  /*
  * Ürünün fiyat alanı burada oluşturuluyor.
  * Eğer varsa indirim oranına göre fiyat yeniden hesaplanıyor.
  * Ayrıca ek indirim bilgisi de tanımlanıyor.
  */
  renderPrice = () => {
    let discount = this.props.info.discount
    let render = <div></div>
    let price = this.props.info.price
    if (discount && discount > 0) {
      let oldPrice = price
      price = (price * (100 - discount)) / 100
      render = (
        <div className="flex flex-row">
          <div className="w-24 text-red border border-red rounded-lg text-center font-medium px-3 py-2 m-2">
            <p className="text-lg font-bold">% {discount}</p>
            <p>indirim</p>
          </div>
          <div className="text-gray-700 text-center px-3 m-2">
            <div className="flex flex-col ">
              <div className="text-gray-700 text-center">
                <p className="text-xl line-through">
                  {oldPrice} {"\u20BA"}
                </p>
              </div>
              <div className="text-gray-700 text-center">
                <p className="font-bold text-2xl">
                  {price} {"\u20BA"}
                </p>
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      render = (
        <div className="flex flex-row">
          <div className="text-center py-2 m-2">
            <div className="text-gray-700 text-center">
              <p className="text-2xl font-bold">
                {price} {"\u20BA"}
              </p>
            </div>
          </div>
        </div>
      )
    }
    if (this.props.info.specialDiscount) {
      render = (
        <div className="flex flex-col">
          <div>{render}</div>
          <div>{this.renderSpecialOffer(price)}</div>
        </div>
      )
    }

    return render
  }
/**
 * Ek indirim bilgisinin tanımlanması burada tamamlanıyor.
 */
  renderSpecialOffer = (price) => {
    let specialDiscount = this.props.info.specialDiscount
    let newPrice = (price * (100 - specialDiscount.discount)) / 100
    let specialDiscountRender = (
      <div className="flex flex-row">
        <div className="w-24 text-white border bg-green-500 rounded-lg text-center font-medium px-2 py-2 m-2">
          <p className="text-xl font-bold">% {specialDiscount.discount}</p>
          <p >{specialDiscount.title}</p>
        </div>
        <div className="text-gray-700 text-center font-bold px-4 py-2 m-2">
          <p className="text-3xl">
            {newPrice} {"\u20BA"}
          </p>
        </div>
      </div>
    )
    return specialDiscountRender
  }

  //Onay modalının close listenerı
  onClose = () => {
    this.setState({
      confirmModalStatus: false,
    })
  }
  //onay modalının olumlu sonuç listenerı
  confirmAddBasket = () => {
    this.addToBasket()
  }
  /**
   * Sepete ekleme işlemi için beden kontrolü yapılıyor.
   * Duruma göre kullanıcı uyarılıyor veya onay modalı açılıyor
   */
  askForConfirm = () => {
    if (!this.state.selectedSize) {
      this.props.dispatch(
        initAlert({ message: "Lütfen numara seçimi yapınız", show: true })
      )
    } else {
      this.setState({
        confirmModalStatus: true,
      })
    }
  }

  /**
   * Tanıtım bilgisi için, ana bilgilendirme alanında veri çekiliyor.
   */
  renderMiniInfo = () => {
    return this.props.info.mainInfo.split(".")[0]
  }

  render() {
    let info = this.props.info
    return (
      <Card>
        <div className="m-2">
          <h3 className="text-3xl font-semibold">{info.name}</h3>
          <p className="text-sm">{this.renderMiniInfo()}</p>
        </div>

        {this.renderPrice()}

        <SizeAndStock
          stokInfo={info.size}
          onChange={(event) => {
            this.sizeChanged(event)
          }}
        ></SizeAndStock>

        <button
          onClick={() => {
            this.askForConfirm()
          }}
          className="w-full bg-red p-3 m-2 rounded-lg text-center font-semibold text-xl text-white focus:outline-none focus:shadow-outline"
        >
          <svg
            className="fill-current mr-3 text-white inline-block h-6 w-6"
            xmlns="http://www.w3.org/2000/svg"
            enableBackground="new 0 0 24 24"
            viewBox="0 0 24 24"
            fill="black"
            width="18px"
            height="18px"
          >
            <g>
              <rect fill="none" height="24" width="24" />
              <path d="M18,6h-2c0-2.21-1.79-4-4-4S8,3.79,8,6H6C4.9,6,4,6.9,4,8v12c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V8C20,6.9,19.1,6,18,6z M10,10c0,0.55-0.45,1-1,1s-1-0.45-1-1V8h2V10z M12,4c1.1,0,2,0.9,2,2h-4C10,4.9,10.9,4,12,4z M16,10c0,0.55-0.45,1-1,1 s-1-0.45-1-1V8h2V10z" />
            </g>
          </svg>
          Sepete Ekle
        </button>
        <ConfirmModal
          show={this.state.confirmModalStatus}
          selectedSize={this.state.selectedSize}
          finalPrice={this.state.price}
          info={this.props.info}
          onClose={() => this.onClose()}
          onConfirm={() => {
            this.confirmAddBasket()
          }}
        ></ConfirmModal>
      </Card>
    )
  }
}

const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps)(Info)
