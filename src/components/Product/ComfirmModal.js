import React from "react"
import Modal from "react-modal"

const _dimensions = { width: window.innerWidth, height: window.innerHeight }

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    minWidth: (_dimensions.width / 2),
    borderRadius: 10,
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
}

Modal.setAppElement("#root")

//Sepete ekleme işlemi için onay mekanizması

class ConfirmModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      show: false,
      selectedSize: {}
    }
    this.handleClose = this.handleClose.bind(this)
    this.handleConfirm = this.handleConfirm.bind(this)
  }
//props ile aktarılan data sayfa içerisinde update ediliyor.
  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    this.setState({
      show: nextProps.show,
      selectedSize: nextProps.selectedSize
    })
  }

  handleClose = () => {
    this.setState({
      show: false,
    })
    this.props.onClose(true)
  }
  handleConfirm = () => {
      this.setState({
          show: false
      })
      this.props.onClose(true)
      this.props.onConfirm(true)
  }

  render() {
    return (
      <>
        <Modal
          isOpen={this.state.show}
          onRequestClose={this.handleClose}
          style={customStyles}
          contentLabel="Sepete Eklemeyi Onaylıyor musunuz?"
        >
          <div className="flex flex-col">
            <div className="text-black text-left px-4 py-2 m-2">
              {this.props.info.name}
            </div>
            <div className="text-black text-left  px-4 py-2 m-2">
              {this.state.selectedSize ? this.state.selectedSize.id + " Numara" : "Numara Seçilemedi."}
            </div>
            <div className="px-2 py-1 mx-1">
              <button className="w-full px-2 py-1 border-red-700 border-2 rounded-lg text-red font-semibold" onClick={()=>this.handleClose()}>Vazgeç</button>
            </div>
            <div className="px-2 py-1 mx-1">
              <button className="w-full px-2 py-1 text-white text-lg rounded-lg bg-green-600 font-semibold" onClick={() => this.handleConfirm()}>{this.props.finalPrice} {"\u20BA"} - Onayla</button> 
            </div>
          </div>
        </Modal>
      </>
    )
  }
}
export default ConfirmModal
