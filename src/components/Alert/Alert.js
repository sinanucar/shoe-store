import React from "react"
import { connect } from "react-redux"
import store from "../../store"

class Alert extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      show: false,
      message: "",
    }
  }

  render() {
    store.subscribe((result) => {
      console.log(result)
      if (this.props.alert.alert) {
        this.setState({
          show: this.props.alert.alert.show,
          message: this.props.alert.alert.message,
        })
        setTimeout(() => {
          this.setState({
            show: false,
            message: ""
          })
        }, 5000);
      }
    })
    return (
      <div className={this.state.show ? "fixed bottom-0 right-0" : "hidden"}>
        <div
          className="bg-red-200 min-w border-l-4 border-red text-black px-4 py-3 m-4 rounded"
          role="alert"
        >
          {this.state.message}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps)(Alert)
// export default Alert
