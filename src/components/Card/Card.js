import React from "react"

class Card extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
        <div className={["bg-white p-6 rounded-lg shadow-lg"]}>
          {this.props.children}
        </div>
      </div>
    )
  }
}
export default Card
